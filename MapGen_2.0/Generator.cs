﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapGen_2._0
{
    namespace Generation
    {

        #region Structs used in generator class
        struct map_field
        {
            public byte height; //height of the pixel
            public byte level_group; //level group of pixel; same as initial height
        }

        struct seed_field
        {
            public int x;
            public int y;
            public byte height;
        }

        struct map_properties
        {
            public byte highs_offset; //shift of the higher parts of map 
            public byte valleys_offset; //shift of the lower parts of map
            public byte ground_offset; //shift of the base part of map
            public byte offset; //shift of all levels of map

            public int map_size; //store size of the map
            public byte max_height; //specify highest point on the map
            public byte min_height; //specify lowest point on the map
            public float layer_size; //base size of every layer
            public float[] layer_multiplier; //multiplier of next layer size

            public int positive_seed_count; //specify count of the highs on map
            public int negative_seed_count; //specify count of the lows on map
        }
        #endregion
        class Generator
        {
            #region class variables and objects
            private int map_size; 

            private map_field[,] heighmap;
            private map_properties properties;
            private List<seed_field> seed = new List<seed_field>();
            private Random rand = new Random();
            #endregion

            /// <summary>
            /// Default constructor. Initialize map properties with dfault values.
            /// </summary>
            /// <param name="map_size">Size of the map</param>
            public Generator(int map_size)
            {
                this.map_size = properties.map_size = map_size;
                heighmap = new map_field[map_size, map_size];
                ClearMap();
            }

            /// <summary>
            /// This function resets map to its default settings.
            /// </summary>
            public void ClearMap()
            {
                //Clear offset
                properties.offset = 0;
                properties.ground_offset = 0;
                properties.highs_offset = 0;
                properties.valleys_offset = 0;
                for(int i = 0; i < map_size; i++)
                {
                    for(int j = 0; j < map_size; j++)
                    {
                        heighmap[i, j].height = 0;
                        heighmap[i, j].level_group = 0;
                    }
                }
                seed.Clear();
            }

            private void GenerateSeed(int quantity, byte height)
            {
                seed_field current_field;
                for (int i = 0; i < quantity; i++)
                {
                    int x, y; //local variables representing coordinates of each seed
                    bool is_too_close; // bool variable controling loop. True if seeds are to close with each other
                    do
                    {
                        is_too_close = false;
                        //  Generating random coordinates for seed
                        x = rand.Next(map_size);
                        y = rand.Next(map_size);
                        // check distance between seeds ( they should be higher than size of first layer )
                        foreach (seed_field s in seed)
                        {
                            // Breaking loop and redrawing seed coordinates if seed is too close to others
                            if (s.x > x)
                            {
                                if (s.x - x < properties.layer_size)
                                {
                                    is_too_close = true;
                                    break;
                                }
                            }
                            else if (s.x < x)
                            {
                                if (x - s.x < properties.layer_size)
                                {
                                    is_too_close = true;
                                    break;
                                }
                            }
                            if (s.y > y)
                            {
                                if (s.y - y < properties.layer_size)
                                {
                                    is_too_close = true;
                                    break;
                                }
                            }
                            else if (s.y < y)
                            {
                                if (y - s.y < properties.layer_size)
                                {
                                    is_too_close = true;
                                    break;
                                }
                            }
                        }
                    } while (is_too_close);
                    current_field.x = x;
                    current_field.y = y;
                    current_field.height = height;
                    seed.Add(current_field);
                }
            }

            private void GenerateFirstLevel()
            {
                foreach(seed_field s in seed)
                {
                    //generate field of random size around seed
                }
            }

            #region Setters
            /// <summary>
            /// Sets maximum height for generator.
            /// </summary>
            /// <param name="max_height">This float represent maximal height in the generation process.</param>
            public void SetMaxHeight(byte max_height)
            {
                properties.max_height = max_height;
            }
            /// <summary>
            /// Sets minimal height for generator.
            /// </summary>
            /// <param name="min_height">This float represent minimal height in the generation process.</param>
            public void SetMinHeight(byte min_height)
            {
                properties.min_height = min_height;
            }
            /// <summary>
            /// Sets base size for each layer.
            /// </summary>
            /// <param name="layer_size">This float represents base size for each layer.</param>
            public void SetLayerSize(float layer_size)
            {
                properties.layer_size = layer_size;
            }
            /// <summary>
            /// This method sets size multiplier for each layer.
            /// </summary>
            /// <param name="layer_multiplier">This float incerase each multiplauyer by each layer</param>
            public void SetLayermultiplier(float layer_multiplier)
            {
                // TO DO: Think about way to make non-linear multipliers!
                float tmp = 1.0f;
                for (int i = 0; i < properties.max_height; i++)
                {
                    properties.layer_multiplier[i] = tmp = tmp += layer_multiplier;
                }
            }
            #endregion
            #region Getters
            public byte[,] GetMap()
            {
                byte[,] map = new byte[map_size, map_size];
                for (int i = 0; i < map_size; i++)
                {
                    for (int j = 0; j < map_size; j++)
                    {
                        map[i, j] = heighmap[i, j].height;
                    }
                }
                return map;
            }
            #endregion
        }
    }
}
