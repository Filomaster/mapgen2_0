﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Color = System.Drawing.Color;

namespace MapGen_2._0
{
	/// <summary>
	/// Interaction logic for class MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{

		private const int MAP_SIZE = 1000;
		private const string VERSION = "v.2.1.5a";
        bool isMapGenerated = false;
		BitmapImage splash = new BitmapImage();
        Bitmap displayed;

		private GraphicManager graphic = new GraphicManager(MAP_SIZE, Palette.color_14);
		private MyLib.Generator.Generator generator;
        private List<string> palette_options = new List<string>();

		public MainWindow()
		{
            //Sets every possible 
            foreach (Palette p in (Palette[])Enum.GetValues(typeof(Palette)))
            {
                palette_options.Add(Enum.GetName(typeof(Palette), p).ToUpper().Replace('_',' '));
            }
            
			splash.BeginInit();
			splash.UriSource = new Uri(@"Images/default.bmp", UriKind.RelativeOrAbsolute);
			splash.EndInit();

            
			generator = new MyLib.Generator.Generator(MAP_SIZE);

			generator.SetMaxHeight(14);
			generator.SetOceansCount(30);
			generator.SetOceansSize(500000);
			
			InitializeComponent();
			Title = "MapGen " + VERSION;
			Display.Source = splash;
			
            // Sets list of all palettes as combobox options, and select first
            PalettePicker.ItemsSource = palette_options;
            PalettePicker.SelectedIndex = 0;

		}
		
		private void GeneratorBtn_Click(object sender, RoutedEventArgs e)
		{
			#if DEBUG
			var watch = System.Diagnostics.Stopwatch.StartNew();
            var main_watch = System.Diagnostics.Stopwatch.StartNew();
			#endif
			generator.GenerateMap_T();
			watch.Stop();
			#if DEBUG
			System.Diagnostics.Debug.WriteLine("Succesfully generated map! Process took " + watch.ElapsedMilliseconds + " ms");
            displayed = graphic.CreateBitmap(generator.GetMap());
            Display.Source = graphic.ConvertBitmapToSource(displayed);
            isMapGenerated = true;
            main_watch.Stop();
            System.Diagnostics.Debug.WriteLine("Application succesfully stopped after " + main_watch.ElapsedMilliseconds + " ms");
            System.Diagnostics.Debug.WriteLine("-----------------------------------------------------");
			#endif
		}

        private void PalettePicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            graphic.SetPalette(PalettePicker.SelectedIndex);
            if(isMapGenerated)
                Display.Source = graphic.ConvertBitmapToSource(graphic.DimmBitmap(displayed));
        }

        private void XD_Click(object sender, RoutedEventArgs e)
        {
            if (isMapGenerated)
                Display.Source = graphic.ConvertBitmapToSource(graphic.DimmBitmap(displayed));
        }
    }
}
