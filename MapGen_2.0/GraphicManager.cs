﻿using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Color = System.Drawing.Color;
using PixelFormat = System.Drawing.Imaging.PixelFormat;

namespace MapGen_2._0
{
    /// <summary>
    /// Enum used to specify which color palette will be used in map creation.
    /// </summary>
    public enum Palette { color_14, mono_14/*palette_50*/ }

    public class GraphicManager
    {
        /// <summary>
        /// Palette of colors which contains colors for all posiblle generator values.
        /// </summary>
        public static Color[][] palette =
        {
            new Color[]
            {
                ColorTranslator.FromHtml("#D9D9D9"),
                ColorTranslator.FromHtml("#FF0084D6"),
                ColorTranslator.FromHtml("#ff0993bf"),
                ColorTranslator.FromHtml("#ff10a79c"),
                ColorTranslator.FromHtml("#ff139e7e"),
                ColorTranslator.FromHtml("#ff1c7d25"),
                ColorTranslator.FromHtml("#ff1c7d25"),
                ColorTranslator.FromHtml("#ff489f20"),
                ColorTranslator.FromHtml("#ff489f20"),
                ColorTranslator.FromHtml("#fff2ec58"),
                ColorTranslator.FromHtml("#FFFCC43E"),
                ColorTranslator.FromHtml("#FFFF6D13"),
                ColorTranslator.FromHtml("#FFFF4303"),
                ColorTranslator.FromHtml("#FF1000"),
                ColorTranslator.FromHtml("#FF720F00")
            },
            new Color[]
            {
                ColorTranslator.FromHtml("#D9D9D9"),
                ColorTranslator.FromHtml("#1A1A1A"),
                ColorTranslator.FromHtml("#282828"),
                ColorTranslator.FromHtml("#3A3A3A"),
                ColorTranslator.FromHtml("#4B4B4B"),
                ColorTranslator.FromHtml("#5B5B5B"),
                ColorTranslator.FromHtml("#6B6B6B"),
                ColorTranslator.FromHtml("#7B7B7B"),
                ColorTranslator.FromHtml("#8B8B8B"),
                ColorTranslator.FromHtml("#9B9B9B"),
                ColorTranslator.FromHtml("#ACACAC"),
                ColorTranslator.FromHtml("#FFB5B5B5"),
                ColorTranslator.FromHtml("#FFCBCBCB"),
                ColorTranslator.FromHtml("#BCBCBC"),
                ColorTranslator.FromHtml("#CCCCCC")
            }
        };

        private int map_size;
        private Color[] used_palette = palette[0];

		/// <summary>
		/// Default constructor for GraphicManager class
		/// </summary>
		/// <param name="map_size"> Specifies size of the bitmap; Must be identic as generated map size</param>
		/// <param name="palette"> Specifies number of colors used in map creation</param>
        public GraphicManager(int map_size, Palette p)
        {
            this.map_size = map_size;
            this.used_palette = palette[(int)p];
        }
		/// <summary>
		/// This function generate bitmap from given map ( 2D array of type int[,] )
		/// </summary>
		/// <param name="map">Get 2-dimensional array representing height of the terrarin</param>
		/// <returns>Return generated bitmap</returns>
        public Bitmap CreateBitmap(int[,] map)
        {
			#if DEBUG
			System.Diagnostics.Debug.WriteLine("Started bitmap generation.");
			var watch = System.Diagnostics.Stopwatch.StartNew();
			#endif

			//Get array of height of every pixel in map
			int[] heighmap_array = ConvertMapToArray(map);
            //Create bitmaps for further processing. Bitmaps are in 24-bit mode, so we get 8 bit for each color.
            Bitmap bitmap = new Bitmap(map_size, map_size);
            BitmapData bitmapData = bitmap.LockBits( new Rectangle(0, 0, map_size, map_size), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
            //Get adres of the first line
            IntPtr ptr = bitmapData.Scan0;
            //Array to hold pixel RGB values
            int bytes = Math.Abs(bitmapData.Stride) * bitmap.Height; //
            byte[] rgbValues = new byte[bytes];
            //Copy RGB values to array
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);
            //This piece of code sets color of every pixel in bitmap
            int counter = 0;
            for (int i = 0; i < rgbValues.Length; i += 3) //Main loop, setting every pixel color. New pixel starts at every 3 bytes
            {
				//Gets current pixel heigh, and it's color split to red, green and blue values.
                int current_pixel = heighmap_array[counter]; 
                rgbValues[i + 0] = used_palette[current_pixel].B;
                rgbValues[i + 1] = used_palette[current_pixel].G;
                rgbValues[i + 2] = used_palette[current_pixel].R;
                counter++;
            }
            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, bytes); // Copy RGB Values back to bitmap
            bitmap.UnlockBits(bitmapData); //Unlock locked piece of bitmap to en the process

			#if DEBUG
			watch.Stop();
			System.Diagnostics.Debug.WriteLine("Bitmap succesfully generated after " + watch.ElapsedMilliseconds + " ms");
			#endif
			//Return generated bitmap
			return bitmap;
        }

        public Bitmap DimmBitmap(Bitmap bmp)
        {
            BitmapData bitmapData = bmp.LockBits(new Rectangle(0, 0, map_size, map_size), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            //Get adres of the first line
            IntPtr ptr = bitmapData.Scan0;
            //Array to hold pixel RGB values
            int bytes = Math.Abs(bitmapData.Stride) * bmp.Height; //
            byte[] rgbValues = new byte[bytes];
            //Copy RGB values to array
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);
            //This piece of code sets color of every pixel in bitmap
            int counter = 0;
            for (int i = 0; i < rgbValues.Length; i += 3) //Main loop, setting every pixel color. New pixel starts at every 3 bytes
            {
                //Gets current pixel heigh, and it's color split to red, green and blue values.
                int B = rgbValues[i + 0];
                int G = rgbValues[i + 1];
                int R = rgbValues[i + 2];
                byte RGB = (byte)(0.3*R + 0.59*G + 0.11*B);
                //byte bR, bG, bB;
                //bR = (byte)(R += R > 50 ? -50 : 0);
                //bG = (byte)(G += G > 50 ? -50 : 0);
                //bB = (byte)(B += B > 50 ? -50 : 0);
                rgbValues[i + 0] = RGB; //(byte)(RGB + B/2); //bB;
                rgbValues[i + 1] = RGB; //(byte)(RGB + G/2); //bG;
                rgbValues[i + 2] = RGB; //(byte)(RGB + R/2); //bR;
                counter++;
            }
            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, bytes); // Copy RGB Values back to bitmap
            bmp.UnlockBits(bitmapData); //Unlock locked piece of bitmap to en the process

            return bmp;
        }

        /// <summary>
        /// This class converts bitmap image from class System.Drawing to ImageSource from System.Media.Imaging.
        /// </summary>
        /// <param name="map_bitmap">Get bitmap image to convert.</param>
        /// <returns>Return onverted ImageSource</returns>
        public ImageSource ConvertBitmapToSource(Bitmap map_bitmap)
		{
			BitmapImage bitmap = new BitmapImage();
			MemoryStream stream = new MemoryStream();

			#if DEBUG
			System.Diagnostics.Debug.WriteLine("Started converting a bitmap");
			var watch = System.Diagnostics.Stopwatch.StartNew();
			#endif

			// Save bitmap to memory stream
			map_bitmap.Save(stream, ImageFormat.Bmp);
			bitmap.BeginInit();
			bitmap.StreamSource = stream;
			bitmap.EndInit();
			bitmap.Freeze();

			#if DEBUG
			watch.Stop();
			System.Diagnostics.Debug.WriteLine("Converting bitmap took " + watch.ElapsedMilliseconds + " ms");
			#endif
			
			return bitmap;
		}

		/// <summary>
		/// This class converts map generated by Generator class from 2-dimensional to one-dimensional array.
		/// </summary>
		/// <param name="map"> Get 2D map array</param>
		/// <returns>Return array of the all pixels</returns>
		private int[] ConvertMapToArray(int[,] map)
		{
			#if DEBUG
			System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
			System.Diagnostics.Debug.WriteLine("Started convert of map");
			watch.Start();
			#endif
			int[] array = new int[map_size * map_size]; // Create array of size equals square of map size, which contains all pixels.
			int counter = 0;
			for (int i = 0; i < map_size; i++)
			{
				for (int j = 0; j < map_size; j++)
				{
					array[counter] = map[i, j];
					counter++;
				}
			}
			#if DEBUG
			watch.Stop();
			System.Diagnostics.Debug.WriteLine("Finished. Enqueued " + counter + " items");
			System.Diagnostics.Debug.WriteLine("Converting took " + watch.ElapsedMilliseconds + " ms");
			#endif
			return array;
		}
        #region Setters
        public void SetPalette(Palette p) { this.used_palette = palette[(int)p]; }
        public void SetPalette(int i) { this.used_palette = palette[i]; }
        #endregion
    }
}
